# Phobiapp

A native Android application to help people with arachnophobia by exposing them to various levels of stimuli, 
and through exposure therapy help them be more comfortable with spiders, through puzzle style games and 'levelling up' 
